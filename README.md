## Ontology based model transformation to Asset Administration Shells
Asset Administration Shells based on the version 3.0VRC02 of the meta-model.

* For Installing conda:
 https://docs.conda.io/projects/miniconda/en/latest/

### Commands
Either run the provided shell script

```
$ bash build_env.sh
$ bash execute.sh
```

OR


```
$ conda env create -f environment.yml
$ conda activate emkoi_test0
```

* For the execution of the codes

```
$ python ./src/main.py

```
