# A module with different utility functionalities

# std imports
import sys
import os
import json
from typing import List, Text, Dict

# 3rd party imports
import aas_core3.types as aas_types
import pycurl
from dotenv import load_dotenv
from owlready2 import get_ontology, namespace, ThingClass

# local imports
from helpers.data_types_matching import data_type_converter

sys.path.append(os.path.join(sys.path[0],))


def extract_ontology(
        ontology_file_path: Text) -> namespace.Ontology:
    """
    a function that loads an ontology from a local file
    to the current workspace
    """

    onto = get_ontology(ontology_file_path).load()
    # onto = get_ontology("./data/pizza.owl").load()
    print("ontology imported :", onto.get_namespace)
    return onto


def create_submodels_from_individuals(
        onto: namespace.Ontology,
        individual_list: List[ThingClass]) -> aas_types.Environment:
    """
    a function to create submodels from group of properties
    from individuals
    """

    # list_of_data_properties = [str(prop) for prop in onto.data_properties()]
    list_of_all_data_properties = list(onto.data_properties())
    list_of_data_properties = []

    # consider only general properties
    for selected_property in list_of_all_data_properties:
        if onto.hasGeneralCapability in selected_property.is_a:
            list_of_data_properties.append(selected_property)
        else:
            # does not append
            pass

    # TODO: consier other properties in the logic

    elements_list = []
    submodel_list = []

    for individual in individual_list:
        individual_label = str(individual)

        for data_property in list_of_data_properties:
            if len(data_property.label) != 0:
                # data_property_value = \
                # individual.__dict__.get(str(data_property.label[0]))
                try:
                    data_property_value = getattr(
                        individual, str(data_property.label[0]))
                except AttributeError:
                    data_property_value = [None]
            else:
                data_property_value = [None]

            # if type(data_property_value)
            # == (list or owlready2.prop.IndividualValueList):
            try:
                if len(data_property_value) == 1:
                    data_property_value = data_property_value[0]
                elif len(data_property_value) > 1:
                    data_property_value = data_property_value
                else:
                    data_property_value = None
            except TypeError:
                data_property_value = data_property_value

            # Create an element
            if data_property_value is not None:
                element = aas_types.Property(
                    id_short=str(data_property),
                    # TODO complete data types
                    value_type=data_type_converter(data_property),
                    # Swagger - API has a problem/limitation of json deserialization
                    # ..../aasxserver/swagger/index.html
                    # in this implementation
                    # therefore here, we need to give the values in a string
                    # otherwise it gives an error
                    # then FROM the AAS - this appears without errors
                    value=str(data_property_value)
                )
            else:
                element = aas_types.Property(
                    id_short=str(data_property),
                    # TODO complete data types
                    value_type=data_type_converter(data_property),
                    value=data_property_value
                )
            elements_list.append(element)

        # Nest the elements in a submodel
        submodel = aas_types.Submodel(
            id=individual_label,
            submodel_elements=elements_list
        )

        submodel_list.append(submodel)

    # Now create the environment to wrap it all up
    environment = aas_types.Environment(
        submodels=submodel_list
    )

    return environment


def aasxserver_update(
        data: Dict) -> Text:
    """
    a function that updates aasx webserver via http
    """
    data = json.dumps(data)

    load_dotenv()

    AASSUB_URL = os.getenv('AASSUB_URL')

    c = pycurl.Curl()
    c.setopt(pycurl.VERBOSE, 1)
    c.setopt(pycurl.URL, AASSUB_URL)
    c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json', 'Accept: application/json'])
    c.setopt(pycurl.CUSTOMREQUEST, "PUT")
    c.setopt(pycurl.POSTFIELDS, data)
    c.perform()
