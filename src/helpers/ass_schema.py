# A module that contains examples for additional descriptions for AAS


# According to AasCore.Aas3_0.Submodel schema
additional_submodel_details = {
    "idShort": "emkoi.802_11n_1Antenna_20MHz",
    "displayName": [
        {
            "language": "en",
		    "text": "test 001"
	    }
    ],
    "description": [
        {
            "language": "en",
            "text": "test 001"
	    }
    ],
    "administration": {
        "version": "001",
        "revision": "001",
        "templateId": "001"
    },
    "kind": "Instance",
}
