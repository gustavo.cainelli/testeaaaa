# A helper module to match data types of ontologies and AAS

# std imports

# 3rd party imports
import aas_core3.types as aas_types
from owlready2 import DataProperty

# local imports


def data_type_converter(
        data_property: DataProperty) -> aas_types.DataTypeDefXSD:

    """
    A helper function to match datatypes
    """

    data_property = str(data_property)
    if data_property == 'emkoi.averageLatency':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.minLatency':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.guaranteedLatency':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.addsOverHead':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.hasDFS':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.dampeningOfSignalPerMeter':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.eavesdroppingPotential':
        return aas_types.DataTypeDefXSD.STRING

    elif data_property == 'emkoi.handlesPacketLoss':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.hasAllowanceForRegulatoryDomain':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.maxDataBandwidth':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.maxEnergyConsumption':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.maxLinkRange':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.maxNumberOfStations':
        return aas_types.DataTypeDefXSD.INTEGER

    elif data_property == 'emkoi.maxTypicalLinkRange':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.maxUserDataLength':
        return aas_types.DataTypeDefXSD.INTEGER

    elif data_property == 'emkoi.minCommunicationCycle':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.minTransferInterval':
        return aas_types.DataTypeDefXSD.INTEGER

    elif data_property == 'emkoi.requiredTimeToStartCommunicating':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.signalModulationScheme':
        return aas_types.DataTypeDefXSD.STRING

    elif data_property == 'emkoi.supportsSeamlessStationHandover':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.typicalDataBandwidth':
        return aas_types.DataTypeDefXSD.INTEGER

    elif data_property == 'emkoi.typicalEnergyConsumption':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.typicalNumberOfStation':
        return aas_types.DataTypeDefXSD.INTEGER

    elif data_property == 'emkoi.maxAllowedTxPower':
        return aas_types.DataTypeDefXSD.FLOAT

    elif data_property == 'emkoi.supportsSNMP':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.usesBandwidthReservation':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.hasLowerLimit2_4GHz':
        return aas_types.DataTypeDefXSD.BOOLEAN

    elif data_property == 'emkoi.hasLowerLimit5GHz':
        return aas_types.DataTypeDefXSD.BOOLEAN
    elif data_property == 'emkoi.hasLowerLimit6GHz':
        return aas_types.DataTypeDefXSD.BOOLEAN
    elif data_property == 'emkoi.hasUpperLimit2_4GHz':
        return aas_types.DataTypeDefXSD.BOOLEAN
    elif data_property == 'emkoi.hasUpperLimit5GHz':
        return aas_types.DataTypeDefXSD.BOOLEAN
    elif data_property == 'emkoi.hasUpperLimit6GHz':
        return aas_types.DataTypeDefXSD.BOOLEAN
    elif data_property == 'emkoi.hasBaseFrequency':
        return aas_types.DataTypeDefXSD.FLOAT
    else:
        return aas_types.DataTypeDefXSD.FLOAT
