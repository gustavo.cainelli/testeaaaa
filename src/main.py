# The Main module

# std imports
import logging as logs
import sys
import os
import json

# 3rd party imports
import aas_core3.jsonization as aas_jsonization
# import aas_core3_rc02.xmlization as aas_xmlization

# local imports
from helpers.ass_schema import additional_submodel_details
from utils import extract_ontology
from utils import create_submodels_from_individuals
from utils import aasxserver_update

sys.path.append(os.path.join(sys.path[0],))


def main() -> None:
    """
    the function that executes the entire process
    """

    logs.basicConfig(filename="./log/main.log", level=logs.DEBUG)
    logs.info("Started")

    emkoi_ontology = extract_ontology("./data/emkoi.owl")
    print("classes: \n", list(emkoi_ontology.classes()))

    individuals = list(emkoi_ontology.individuals())

    sub_environment1 = create_submodels_from_individuals(
        emkoi_ontology,
        [individuals[6], individuals[7]])

    print("a submodel environemt is created: \n", sub_environment1)

    jsonable_sub_environment1 = aas_jsonization.to_jsonable(sub_environment1)

    with open("./data/aas_core_data.json", "w", encoding="utf-8") as f:
        json.dump(jsonable_sub_environment1, f, ensure_ascii=False, indent=4)

    test_importable_submodel = jsonable_sub_environment1["submodels"][0]
    test_importable_submodel.update(additional_submodel_details)
    with open("./data/test_importable_submodel.json", "w", encoding="utf-8") as f:
        json.dump(
            test_importable_submodel,
            f, ensure_ascii=False, indent=4)

    # xml_sub_environment1 = aas_xmlization.to_str(sub_environment1)

    # with open("./data/aas_core_data.xml", "w", encoding='utf-8') as f:
    #    f.write(xml_sub_environment1)

    print("type = ", type(test_importable_submodel))
    # update the current AAS server existing submodel automatically
    aasxserver_update(test_importable_submodel)
    print("server submodel updated ...")

    logs.info("Finished")


if __name__ == '__main__':
    # execute only if run as a script
    main()
